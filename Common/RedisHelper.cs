using System;
using StackExchange.Redis;
using Newtonsoft.Json;
using System.Collections.Generic;
using fatmamba.Models;

namespace fatmamba.Common
{
    public class RedisHelper
    {
        private readonly IDatabase _db;

        public RedisHelper()
        {
            _db = Startup._redis.GetDatabase();
        }

        public int GetDatabase()
        {
            return _db.Database;
        }

        public bool UserExist(string userId)
        {
            return _db.KeyExists(userId);
        }

        public void AddOrUpdateUser(string userId, int voteWeight)
        {
            _db.StringSet(userId, voteWeight);
        }

        public void AddOrUpdateUserBattleId(string userId, string battleId)
        {
            _db.StringSet(userId, battleId);
        }

        public int GetUserVoteWeight(string userId)
        {
            return Convert.ToInt32(_db.StringGet(userId));
        }

        public void AddVoteForArtist(string artist, int voteWeight)
        {
            _db.StringIncrement(artist + "Score", voteWeight);
        }

        public void AddOrUpdateArtist(Artist artist)
        {
            var data = JsonConvert.SerializeObject(artist);
            
            if (String.IsNullOrEmpty(artist.ArtistId))
            {
                var rand = new Random();
                artist.ArtistId = rand.Next(1, 1000).ToString();
                AddToArtistSet(artist.ArtistId);
            }

            _db.StringSet(artist.ArtistId, data);
        }

        internal int GetArtistTwoScore()
        {
            return Convert.ToInt32(_db.StringGet("artistTwoScore"));
        }

        internal int GetArtistOneScore()
        {
            return Convert.ToInt32(_db.StringGet("artistOneScore"));
        }

        internal void SetArtistTwoScore(int score)
        {
            _db.StringSet("artistTwoScore", score);
        }

        internal void SetArtistOneScore(int score)
        {
            _db.StringSet("artistOneScore", score);
        }

        public void AddToArtistSet(string artistId)
        {
            _db.SetAdd(ConfigHelper.ArtisitList, artistId);
        }

        public List<string> GetArtistSet()
        {
            List<string> artistList = new List<string>();
            var artists = _db.SetMembers(ConfigHelper.ArtisitList);

            foreach (var artist in artists)
                artistList.Add(artist);

            return artistList;
        }

        internal string GetUserBattleId(string userId)
        {
            return _db.StringGet(userId);
        }

        public Artist GetArtist(string artistId)
        {
            return JsonConvert.DeserializeObject<Artist>(_db.StringGet("artist" + artistId));
        }

        public Battle GetBattle(string battleId)
        {
            return JsonConvert.DeserializeObject<Battle>(_db.StringGet("battle" + battleId));
        }

        public string GetCurrentBattleId()
        {
            return _db.StringGet("currBattleId");
        }

        public string GetBattleStatus()
        {
            return _db.StringGet("battleTimerStatus");
        }

        public TimeSpan GetBattleTimer()
        {
            return TimeSpan.FromSeconds(Convert.ToInt32(_db.StringGet("timeLeft")));
        }

        public void AddOrUpdateBattleWInner(string battleId, string artistId)
        {
            _db.StringSet("Battle" + battleId + "Winner", artistId);
        }

        public int GetBattleWinner(string battleId)
        {
            return Convert.ToInt32(_db.StringGet("Battle" + battleId + "Winner"));
        }

        public void AddBattleArtistOne(string battleId, string artistId)
        {
            _db.StringSet("Battle" + battleId + "ArtistOne", artistId);
        }

        public int GetBattleArtistOne(string battleId)
        {
            return Convert.ToInt32(_db.StringGet("Battle" + battleId + "ArtistOne"));
        }

        public void AddBattleArtistTwo(string battleId, string artistId)
        {
            _db.StringSet("Battle" + battleId + "ArtistTwo", artistId);
        }

        public int GetBattleArtistTwo(string battleId)
        {
            return Convert.ToInt32(_db.StringGet("Battle" + battleId + "ArtistTwo"));
        }
    }
}