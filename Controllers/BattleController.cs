using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using fatmamba.Models;
using fatmamba.Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace fatmamba.Controllers
{
    [Route("api/[controller]")]
    public class BattleController : Controller
    {
        // POST api/battle/getstatus
        [HttpPost("getstatus")]
        public IActionResult GetStatus()
        {
            string requestData = String.Empty;
            using(var reader = new StreamReader(Request.Body))
            {
                requestData = reader.ReadToEnd();
            }

            Passcode passcode = JsonConvert.DeserializeObject<Passcode>(requestData);

            if (passcode.Code != ConfigHelper.Passcode)
            {
                return Unauthorized();
            }

            //TODO: Get Data from Redis
            RedisHelper redis = new RedisHelper();
            string currentBattleId = redis.GetCurrentBattleId();
            Battle battle = redis.GetBattle(currentBattleId);
            battle.BattleStatus = redis.GetBattleStatus();
            battle.TimeLeft = redis.GetBattleTimer();
            battle.ArtistOne.Score = redis.GetArtistOneScore();
            battle.ArtistTwo.Score = redis.GetArtistTwoScore();

            if (battle.ArtistOne.Score > battle.ArtistTwo.Score)
                redis.AddOrUpdateBattleWInner(battle.BattleId, battle.ArtistOne.ArtistId);

            if (battle.ArtistOne.Score < battle.ArtistTwo.Score)
                redis.AddOrUpdateBattleWInner(battle.BattleId, battle.ArtistTwo.ArtistId);

            return Json(battle);
        }

        // POST api/battle/vote
        [HttpPost("vote")]
        public IActionResult Vote()
        {
            // Read data from request
            string requestData = String.Empty;
            using(var reader = new StreamReader(Request.Body))
            {
                requestData = reader.ReadToEnd();
            }

            // Request data has to be formed to match Vote
            Vote vote = JsonConvert.DeserializeObject<Vote>(requestData);

            // Simple Passcode check...
            if (vote.Passcode.Code != ConfigHelper.Passcode)
            {
                return Unauthorized(); //401
            }

            var db = new RedisHelper();
            int userVoteWeight;
            var battleId = db.GetCurrentBattleId();

            //Add User if not exist w/ vote count
            if (db.GetUserBattleId(vote.UserId + "BattleId") != battleId)
            {
                userVoteWeight = 100;
                db.AddOrUpdateUser(vote.UserId, userVoteWeight);

                // This allows us to reset the vote weight per battle
                db.AddOrUpdateUserBattleId(vote.UserId + "BattleId", battleId);
            }
            else
            {
                userVoteWeight = db.GetUserVoteWeight(vote.UserId);
            }

            var battle = db.GetBattle(battleId);
            var artist = (vote.ArtistId == battle.ArtistOne.ArtistId) ? "artistOne" : "artistTwo";

            if (userVoteWeight != 1)
            {
                //Add Vote
                db.AddVoteForArtist(artist, userVoteWeight);

                //Decrement user vote, stops at 1
                userVoteWeight--;
                db.AddOrUpdateUser(vote.UserId, userVoteWeight);

                //TODO: Error decrement or vote
            }
            else 
            {
                //Add Vote of 1
                db.AddVoteForArtist(artist, userVoteWeight);
            }

            //Vote Was Successful
            return Ok(); //200
        }
    }
}